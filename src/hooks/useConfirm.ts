import { useContext } from "react";
import { ConfirmContext } from "../contexts/confirm";

export const useConfirm = () => useContext(ConfirmContext);
