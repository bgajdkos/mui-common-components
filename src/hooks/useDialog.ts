import { useCallback, useState } from "react";

export const useDialog = (initiallyOpen = false) => {
  const [open, setOpen] = useState(initiallyOpen);
  const close = useCallback(
    () => {
      setOpen(false);
    },
    [setOpen],
  );
  const openDialog = useCallback(
    () => {
      setOpen(true);
    },
    [setOpen],
  )

  return { open, close, openDialog };
}
