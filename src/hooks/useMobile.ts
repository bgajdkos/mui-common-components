import { Breakpoint, useMediaQuery, useTheme } from "@mui/material";

export const useMobile = (breakpoint: Breakpoint = "sm") => {
  const theme = useTheme();
  return useMediaQuery(theme.breakpoints.down(breakpoint));
};
