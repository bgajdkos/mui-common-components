export const removeNth = <T>(array: T[], index: number): T[] =>
  array.filter((_, i) => i !== index);
