import { createContext } from "react";
import { DialogConfirmOptionsProps } from "../components/DialogConfirmOptionsProps";

export type ConfirmCallback = (
  options: DialogConfirmOptionsProps
) => Promise<boolean>;
export const ConfirmContext = createContext<ConfirmCallback>(() =>
  Promise.resolve(false)
);
