import { createContext, MouseEventHandler } from "react";

export interface PopoverContextInterface {
  open: boolean;
  anchorEl: HTMLElement | null;
  handleOpen?: MouseEventHandler;
  handleClose?: MouseEventHandler;
}

export const initialPopoverState: PopoverContextInterface = {
  open: false,
  anchorEl: null,
};

export const PopoverContext =
  createContext<PopoverContextInterface>(initialPopoverState);
