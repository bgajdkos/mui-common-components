export interface IdentityProps {
  testid: string;
  id: string;
}

export interface GenericAction<T = any> {
  type: string;
  payload: T;
}

export type ManageActions = "edit" | "delete";
