import { Breakpoint, ButtonProps, Typography } from "@mui/material";
import Button from "@mui/material/Button";
import MuiDialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import Slide from "@mui/material/Slide";
import { TransitionProps } from "@mui/material/transitions";
import * as React from "react";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

type handleCloseCallback = (
  event: {},
  reason: "backdropClick" | "escapeKeyDown" | "cancel"
) => void;
export interface DialogProps {
  open: boolean;
  onClose: handleCloseCallback;
  maxWidthDialog?: Breakpoint;
  fullWidthDialog?: boolean;
  title?: React.ReactNode;
  children?: React.ReactNode;
  renderHeader?: (close: handleCloseCallback) => React.ReactNode;
  renderContent?: (close: handleCloseCallback) => React.ReactNode;
  renderActions?: (close: handleCloseCallback) => React.ReactNode;
  buttons?: {
    label: string;
    closeOnClick: boolean;
    action?: () => void;
    buttonProps?: ButtonProps;
  }[];
}

const Dialog = ({
  open,
  onClose,
  maxWidthDialog = "md",
  fullWidthDialog = true,
  title = "",
  children,
  renderHeader,
  renderContent,
  renderActions,
  buttons = [],
}: DialogProps) => {
  const handleClose = (
    event: {},
    reason: "backdropClick" | "escapeKeyDown" | "cancel"
  ) => {
    onClose(event, reason);
  };

  return (
    <MuiDialog
      TransitionComponent={Transition}
      open={open}
      onClose={handleClose}
      scroll="paper"
      maxWidth={maxWidthDialog}
      fullWidth={fullWidthDialog}
    >
      {renderHeader ? (
        renderHeader(handleClose)
      ) : (
        <DialogTitle>
          {typeof title === "string" ? (
            <Typography component="h2" variant="h5">
              {title}
            </Typography>
          ) : (
            title
          )}
        </DialogTitle>
      )}
      {renderContent ? (
        renderContent(handleClose)
      ) : (
        <DialogContent dividers={true}>{children}</DialogContent>
      )}
      {renderActions ? (
        renderActions(handleClose)
      ) : (
        <DialogActions>
          {buttons.map((btn, index) => (
            <Button
              key={index}
              {...(btn.buttonProps ?? {})}
              onClick={() => {
                if (btn.closeOnClick) {
                  handleClose({}, "cancel");
                }
                btn.action?.();
              }}
            >
              {btn.label}
            </Button>
          ))}
        </DialogActions>
      )}
    </MuiDialog>
  );
};
export default Dialog;
