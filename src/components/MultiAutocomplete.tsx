import MuiAutocomplete, {
  AutocompleteProps as MuiAutocompleteProps,
} from "@mui/material/Autocomplete";
import Chip from "@mui/material/Chip";
import TextField from "@mui/material/TextField";

export interface MultiAutocompleteProps
  extends Omit<
    Partial<MuiAutocompleteProps<string, true, true, true>>,
    "options" | "label" | "value" | "onChange"
  > {
  value: string[];
  options?: string[];
  label: string;
  name?: string;
  id: string;
  onChange: (event: {
    target: { name: string | undefined; value: string[] };
  }) => void;
}

const MultiAutocomplete = ({
  value,
  label,
  id,
  options = [],
  onChange,
  name,
  ...props
}: MultiAutocompleteProps) => {
  return (
    <MuiAutocomplete
      multiple
      id={id}
      options={options}
      value={value}
      freeSolo
      renderTags={(value: readonly string[], getTagProps) =>
        value.map((option: string, index: number) => (
          <Chip variant="outlined" label={option} {...getTagProps({ index })} />
        ))
      }
      renderInput={(params) => (
        <TextField {...params} variant="outlined" label={label} />
      )}
      onChange={(event, newValue) =>
        onChange({ target: { name, value: newValue } })
      }
      {...props}
    />
  );
};

export default MultiAutocomplete;
