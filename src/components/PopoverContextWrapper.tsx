import { MouseEvent, useState } from "react";
import { PopoverContext } from "../contexts";

export interface HandleNameChangeInterface extends MouseEvent {
  target: HTMLInputElement;
}

export interface PopoverContextWrapperProps {
  children: React.ReactNode;
}

const PopoverContextWrapper = ({ children }: PopoverContextWrapperProps) => {
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);
  const handleOpen: React.MouseEventHandler = (
    event: HandleNameChangeInterface
  ) => {
    setAnchorEl(event.target);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <PopoverContext.Provider
      value={{
        open: Boolean(anchorEl),
        anchorEl,
        handleOpen,
        handleClose,
      }}
    >
      {children}
    </PopoverContext.Provider>
  );
};

export default PopoverContextWrapper;
