import { useDialog } from "../hooks/useDialog";
import Dialog from "./Dialog";
import MultiString from "./MultiString";
import { MultiStringProps } from "./MultiStringProps";
import TextButton from "./TextButton";

interface MultiStringDialogProps extends MultiStringProps {
  openDialogLabel: string;
  title: string;
}

const MultiStringDialog = ({
  value,
  onChange,
  name,
  label,
  addButtonLabel,
  openDialogLabel,
  title,
  TextFieldsProps,
}: MultiStringDialogProps) => {
  const { openDialog, open, close } = useDialog();

  return (
    <>
      <TextButton type="button" onClick={openDialog}>
        {openDialogLabel}
      </TextButton>
      <Dialog
        open={open}
        onClose={close}
        title={title}
        buttons={[
          {
            closeOnClick: true,
            label: "Zamknij",
          },
        ]}
      >
        <MultiString
          {...{
            value,
            onChange,
            name,
            label,
            addButtonLabel,
            TextFieldsProps,
          }}
        />
      </Dialog>
    </>
  );
};

export default MultiStringDialog;
