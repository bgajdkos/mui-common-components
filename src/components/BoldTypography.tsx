import { styled, Typography } from "@mui/material";

const BoldTypography = styled(Typography)({
  fontWeight: "bold",
});

export default BoldTypography;
