import Popover from "@mui/material/Popover";
import Paper from "@mui/material/Paper";
import IconButton from "@mui/material/IconButton";
import MoreVertIcon from "@mui/icons-material/MoreVert";
import { ReactNode, useContext } from "react";
import { PopoverContext } from "../contexts";
import { IdentityProps } from "../types";

export interface PopoverMenuInterface {
  children: ReactNode;
}

export const PopoverMenu = (props: PopoverMenuInterface) => {
  const { anchorEl, handleClose } = useContext(PopoverContext);
  return (
    <Popover
      open={Boolean(anchorEl)}
      anchorEl={anchorEl}
      onClose={handleClose}
      anchorOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
      transformOrigin={{
        vertical: "center",
        horizontal: "right",
      }}
    >
      <Paper variant="elevation" sx={{ p: 0.25 }}>
        {props.children}
      </Paper>
    </Popover>
  );
};

export interface MorePopoverButtonProps extends IdentityProps {
  onSelect: (id: string) => void;
}

export const MorePopoverButton = ({
  id,
  testid,
  onSelect,
}: MorePopoverButtonProps) => {
  const { handleOpen } = useContext(PopoverContext);
  return (
    <IconButton
      data-testid={`more-menu-btn-${testid}`}
      sx={{
        transform: "rotateZ(-90deg)",
      }}
      onClick={(evt) => {
        evt.stopPropagation();
        if (handleOpen) {
          handleOpen(evt);
          onSelect(id);
        }
      }}
    >
      <MoreVertIcon />
    </IconButton>
  );
};
