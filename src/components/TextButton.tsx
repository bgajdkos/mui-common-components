import { Button, ButtonProps } from "@mui/material";

const TextButton = (props: Omit<ButtonProps, "disableRipple" | "variant">) => (
  <Button {...props} disableRipple variant="text" />
);

export default TextButton;
