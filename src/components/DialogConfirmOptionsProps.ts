import { DialogProps } from "./Dialog";

export type DialogConfirmOptionsProps = Pick<
  DialogProps,
  "fullWidthDialog" | "title" | "maxWidthDialog"
> &
  Partial<{
    description: string;
    confirmationText: string;
    cancellationText: string;
  }>;
