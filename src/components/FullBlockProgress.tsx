import { Box, CircularProgress } from "@mui/material";

const FullBlockProgress = () => (
  <Box
    sx={{
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      height: "100%",
    }}
  >
    <CircularProgress />
  </Box>
);

export default FullBlockProgress;
