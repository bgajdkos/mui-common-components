import SearchIcon from "@mui/icons-material/Search";
import { InputAdornment, TextField, TextFieldProps } from "@mui/material";

const Search = ({
  label = "Search",
  placeholder = "Search",
  ...props
}: TextFieldProps) => {
  return (
    <TextField
      label={label}
      placeholder={placeholder}
      {...props}
      InputProps={{
        ...(props.InputProps ?? {}),
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
      }}
    />
  );
};

export default Search;
