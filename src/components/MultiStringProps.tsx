import { TextFieldProps } from "@mui/material";

export interface MultiStringProps {
  value: string[];
  onChange: (event: {
    target: { name?: string; value: string | string[] };
  }) => void;
  name: string;
  label: string;
  addButtonLabel: string;
  TextFieldsProps?: Partial<TextFieldProps>[];
}
