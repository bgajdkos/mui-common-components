import { Close } from "@mui/icons-material";
import { IconButton, InputAdornment, TextField } from "@mui/material";
import { useCallback } from "react";
import { removeNth } from "../utils/helpers/removeNth";
import { MultiStringProps } from "./MultiStringProps";
import TextButton from "./TextButton";

const MultiString = ({
  value,
  onChange,
  name,
  label,
  addButtonLabel,
  TextFieldsProps,
}: MultiStringProps) => {
  const inputStrings = value.length === 0 ? [""] : value;
  const handleDelete = useCallback(
    (index: number) => {
      onChange({
        target: {
          name,
          value: removeNth(value, index),
        },
      });
    },
    [onChange, value, name]
  );

  const handleAddAtTheEnd = useCallback(() => {
    onChange({
      target: {
        name,
        value: [...value, ""],
      },
    });
  }, [onChange, value, name]);

  return (
    <>
      {inputStrings.map((str, index) => (
        <TextField
          key={`${name}.${index}`}
          label={`${label} - ${index + 1}`}
          name={`${name}.${index}`}
          multiline
          value={str}
          maxRows={4}
          onChange={onChange}
          fullWidth
          sx={{ mb: 5 }}
          InputProps={{
            endAdornment: value.length > 1 && (
              <InputAdornment position="end">
                <IconButton type="button" onClick={() => handleDelete(index)}>
                  <Close />
                </IconButton>
              </InputAdornment>
            ),
          }}
          {...(TextFieldsProps?.[index] ?? {})}
        />
      ))}
      <TextButton type="button" onClick={handleAddAtTheEnd}>
        {addButtonLabel}
      </TextButton>
    </>
  );
};

export default MultiString;
