import React from "react";
import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import { MorePopoverButtonProps } from "./PopoverElements";
import SimpleCard from "./SimpleCard";

afterEach(cleanup);
describe("Test SimpleCard", () => {
  test("should render", async () => {
    const { container } = render(
      <SimpleCard id="1" testid="1" title="title" />
    );
    expect(container).toBeDefined();
  });
  test("should can callback when onSelect is provided", async () => {
    const mockOnSelect = jest.fn();
    render(
      <SimpleCard
        id="1"
        testid="1"
        title="title"
        description="description"
        onSelect={mockOnSelect}
      />
    );
    expect(screen.getByTestId("simple-card-1")).toBeDefined();
    expect(screen.getByTestId("simple-card-title")).toHaveTextContent("title");
    expect(screen.getByTestId("simple-card-description")).toHaveTextContent(
      "description"
    );

    const card = screen.getByTestId("simple-card-1");
    fireEvent.click(card);
    expect(mockOnSelect).toHaveBeenCalled();
  });

  test("should render Actions when component and onSelectActions callback are provided", async () => {
    const mockOnSelectActions = jest.fn();
    const Actions = ({ id, testid, onSelect }: MorePopoverButtonProps) => (
      <div
        id={id}
        data-testid={`actions-${testid}`}
        onClick={() => onSelect("action1")}
      ></div>
    );
    render(
      <SimpleCard
        id="1"
        testid="1"
        title="title"
        description="description"
        actions={<Actions id="1" onSelect={mockOnSelectActions} testid="1" />}
      />
    );
    expect(screen.getByTestId("simple-card-1")).toBeDefined();
    expect(screen.getByTestId("actions-1")).toBeDefined();
  });
});
