import * as React from "react";
import { Theme, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import OutlinedInput from "@mui/material/OutlinedInput";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, {
  SelectChangeEvent,
  SelectProps as MuiSelectProps,
} from "@mui/material/Select";
import Chip from "@mui/material/Chip";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

function getStyles(item: string, value: readonly string[], theme: Theme) {
  return {
    fontWeight:
      value.indexOf(item) === -1
        ? theme.typography.fontWeightRegular
        : theme.typography.fontWeightBold,
  };
}

export interface MultiSelectProps
  extends Omit<
    Partial<MuiSelectProps<string[]>>,
    "items" | "label" | "value" | "onChange"
  > {
  items: string[];
  label: string;
  value: string[];
  onChange: (value: { target: { value: string[]; name: string } }) => void;
}

const MultiSelect = ({
  items,
  label,
  value,
  onChange,
  ...props
}: MultiSelectProps) => {
  const theme = useTheme();

  const handleChange = (event: SelectChangeEvent<string[]>) => {
    onChange({
      target: {
        value:
          typeof event.target.value === "string"
            ? event.target.value.split(",")
            : event.target.value,
        name: event.target.name,
      },
    });
  };

  return (
    <div>
      <FormControl sx={{ m: 1, width: 300 }}>
        <InputLabel id={`${label}label`}>{label}</InputLabel>
        <Select
          labelId={`${label}label`}
          id={`${label}`}
          multiple
          value={value}
          onChange={handleChange}
          input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
          renderValue={(selected) => (
            <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
              {selected.map((value) => (
                <Chip key={value} label={value} />
              ))}
            </Box>
          )}
          MenuProps={MenuProps}
          {...props}
        >
          {items.map((item: string) => (
            <MenuItem
              key={item}
              value={item}
              style={getStyles(item, value, theme)}
            >
              {item}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default MultiSelect;
