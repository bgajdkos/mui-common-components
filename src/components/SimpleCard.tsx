import { Card, CardContent, Typography } from "@mui/material";
import Stack from "@mui/material/Stack";
import { makeStyles } from "@mui/styles";
export interface SimpleCardProps {
  id: string;
  title: string;
  description?: string;
  testid: string;
  onSelect?: (id: string) => void;
  actions?: React.ReactNode;
}

interface Selectable {
  isSelectable: boolean;
}

const useStyles = makeStyles({
  paper: ({ isSelectable }: Selectable) =>
    isSelectable
      ? {
          "&:hover": {
            cursor: "pointer",
          },
        }
      : {},
});

export const SimpleCard = ({
  id,
  onSelect,
  actions = null,
  ...props
}: SimpleCardProps) => {
  const classes = useStyles({
    isSelectable: Boolean(onSelect),
  });
  const handleClick = onSelect
    ? () => {
        onSelect(id);
      }
    : undefined;
  return (
    <Card
      data-testid={`simple-card-${props?.testid}`}
      className={classes.paper}
      onClick={handleClick}
    >
      <CardContent>
        <Stack
          direction="column"
          spacing={2}
          sx={{
            px: 2,
            py: 1,
          }}
        >
          <Typography
            align="left"
            variant="subtitle1"
            data-testid="simple-card-title"
          >
            {props.title}
          </Typography>
          <Stack
            direction="row"
            justifyContent={props.description ? "space-between" : "flex-end"}
            alignItems="center"
          >
            {props.description && (
              <Typography
                align="left"
                variant="subtitle2"
                data-testid="simple-card-description"
              >
                {props.description}
              </Typography>
            )}
            {actions}
          </Stack>
        </Stack>
      </CardContent>
    </Card>
  );
};

export default SimpleCard;
