import MuiAutocomplete, {
  AutocompleteProps as MuiAutocompleteProps,
} from "@mui/material/Autocomplete";
import TextField from "@mui/material/TextField";
import { ReactNode } from "react";

export interface AutocompleteProps
  extends Omit<
    Partial<MuiAutocompleteProps<string, false, true, false>>,
    "options" | "label" | "value" | "onChange"
  > {
  value: string;
  options: string[];
  label: string;
  name?: string;
  id: string;
  error?: boolean;
  helperText?: ReactNode;
  onChange: (event: {
    target: { name: string | undefined; value: string };
  }) => void;
}

const Autocomplete = ({
  value,
  label,
  id,
  options,
  onChange,
  name,
  error,
  helperText,
  ...props
}: AutocompleteProps) => {
  return (
    <MuiAutocomplete
      id={id}
      options={options}
      value={value}
      renderInput={(params) => (
        <TextField
          {...params}
          {...{ error, helperText }}
          variant="outlined"
          label={label}
        />
      )}
      onChange={(event, newValue) =>
        onChange({ target: { name, value: newValue } })
      }
      {...props}
    />
  );
};

export default Autocomplete;
