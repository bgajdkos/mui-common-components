import {
  Checkbox as MuiCheckbox,
  FormControlLabel,
  FormGroup,
} from "@mui/material";
import React from "react";

interface CheckboxProps {
  value: boolean | null;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  label: string;
  name: string;
}

const Checkbox = ({ value, onChange, label, name }: CheckboxProps) => {
  return (
    <FormGroup>
      <FormControlLabel
        control={
          <MuiCheckbox
            checked={value ?? undefined}
            name={name}
            onChange={onChange}
          />
        }
        label={label}
      />
    </FormGroup>
  );
};

export default Checkbox;
