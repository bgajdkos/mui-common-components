# Material UI sharded components

This package provides shared material components.

## Installation

```sh
npm install --save @itkontekst/mui-common-components
```
